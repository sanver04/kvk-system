DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `dossiernummer` int(16) DEFAULT NULL,
  `handelsnaam` varchar(255) DEFAULT NULL,
  `huisnummer` varchar(255) DEFAULT NULL,
  `huisnummertoevoeging` varchar(255) DEFAULT NULL,
  `plaats` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `straat` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
