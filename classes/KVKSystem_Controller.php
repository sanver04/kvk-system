<?php

class KVKSystem_Controller {

	private $model;
	public $action;

	private $allowActions = array(
		'init',
		'requestAddress'
	);

	public function __construct($model = null, $action = 'init', $params = array()){

		$this->model = (isset($model)) ? $model : null;
		$this->doAction($action, $params);

	}

	public function doAction($action = '', $params = array()) {

		if(method_exists($this, $action) && in_array($action, $this->allowActions)) {
			$this->action = $action;
			call_user_func_array(array($this, $action), array($params));
		} else {
			die('Action not exist or allowed');
		}

	}

	public function __set($key, $value) {
		$this->model->records[$key] = $value;
	}

	public function __get($key) {
		return $this->model->records[$key];
	}

	public function init() {
		
	}

	public function requestAddress($params) {
		
		$dossiernummer = $params['dossiernummer'];
		$address = Address::getByKVK($dossiernummer);

		if(!is_numeric($dossiernummer) || strlen($dossiernummer) < 6) {
			$this->Error = 'Geen geldig dossiernummer.';

			return;
		}

		// Bestaat adres en is deze uptodate?
		if(!$address || !$address->isUpToDate()) {

			$api = new KVKApi('7103a2e621b7354b0fcc27b82d6a3b5c5b5fe1633a16399cc8f109470bb3a51c');
			$result = $api->getKVKInfo($dossiernummer);

			if($result) {
				$data = get_object_vars($result);
	    		if(!$address) {
					$address = new Address();
	    		}
				$address->setRecords($data);
				$address->save();
			} else {
				$this->Error = 'Dossiernummer niet gevonden.';
				return;
			}
		}		

		if($address) {
			// Toon het adres uit de database.
			$view = new View($address);
			$this->Address = $view->output();
		} else {
			$this->Error = 'Geen adres om weer te geven.';
			return;
		}

	}

}