<?php

class Address {

	public $records = array();
	
	private static $dbFields = array(
		'handelsnaam',
		'dossiernummer',
		'straat',
		'huisnummer',
		'huisnummertoevoeging',
		'plaats',
		'postcode'
	);

	private static $con;

	public function __construct(){

	}

	public function output() {
		return new View($this);
	}

	public function __isset($key) {
		return isset($this->records[$key]);
	}

	public function __unset($key) {
		unset($this->records[$key]);
	}

	public function __set($key, $value) {
		$this->records[$key] = $value;
	}

	public function __get($key) {
		if(isset($key)) {
			return $this->records[$key];
		}
	}

	public function setRecords(array $records) {
		foreach($records as $key => $value) {
			if(in_array($key, self::$dbFields)) {				
				$this->records[$key] = $value;				
			}
		}
	}

	private function connect() {
		// Build / Check connection
		self::$con = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if (mysqli_connect_error()) {
			die("Failed to connect to MySQL: " . mysqli_connect_error());
		}
	}

	private function close() {
		self::$con->close();
	}

	public function isUpToDate() {

		$dateModified = date('Y-m-d', strtotime($this->date_modified));
		$today = date('Y-m-d', time());

		if($dateModified == $today) {
			return true;
		} else {
			return false;
		}

	}

	public static function getByKVK($dossiernummer = null) {

		if($dossiernummer === null) {
			return false;
		}

		self::connect();

		$dossiernummer = mysqli_real_escape_string(self::$con, $dossiernummer);

		$sql = "SELECT * FROM Address WHERE dossiernummer = ".$dossiernummer." LIMIT 1";		
		$result = self::$con->query($sql);

		$rows = mysqli_num_rows($result);
		if($rows > 0) {
			return mysqli_fetch_object($result, 'Address');
		}

		return false;
		self::close();

	}


	public function save() {

		self::connect();

		if(isset($this->records['id'])) {
			$item_id = $this->records['id'];
		}
		
		foreach($this->records as $field => $value) {			

			if(is_numeric($value)) {
				$value = (int)$value;
			} elseif(is_string($value)) {
				$value = "'".$value."'";
			} elseif(is_bool($value)) {
				$value = ($value) ? 1 : 0;
			}
			
			if(isset($item_id) && !empty($value) && $field != 'id') {
				$sets[] = $field.'='.$value;
			} else {
				$fields[] = $field;
				$values[] = $value;
			}				

		}


		if(isset($item_id)) {

			$sets[] = "date_modified = NOW()";

			// update		
			$sets = implode(',', $sets);	
			$sql = "UPDATE Address SET ".$sets." WHERE id=".$item_id;
			$result = mysqli_query(self::$con, $sql);
		} else {

			$fields[] = 'date_created';
			$values[] = 'NOW()';
			
			$fields[] = 'date_modified';
			$values[] = 'NOW()';
			
			$fields = implode(',', $fields);
			$values = implode(',', $values);

			$sql = "INSERT INTO Address (".$fields.") VALUES (".$values.")";
			$result = mysqli_query(self::$con, $sql);

		}

		self::close();

		return $result;

	}


}