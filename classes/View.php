<?php

class View {

	private $model;

	public function __construct($model){

		$this->model = (isset($model)) ? $model : null;

	}

	public function __get($key) {
		return $this->model->records[$key];
	}

	public function output() {

		$modelName = get_class($this->model);

		$tplPath = BASE_PATH.'/templates/'.$modelName.'.tpl.php';


		$output = '';
		if(file_exists($tplPath)) {
			ob_start();
			include_once($tplPath);
			$output = ob_get_contents();
			ob_end_clean();			
		} else {
			$output = 'Template not found.';
		}

		return $output;

	}

}