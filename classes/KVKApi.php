<?php

class KVKApi {

	private $developerApiKey = '';

	public function __construct($developerApiKey = null){

		$this->developerApiKey = (isset($developerApiKey)) ? $developerApiKey : '';

	}

	public function getKVKInfo($dossiernummer = null) {

		if($dossiernummer === null) {
			return false;
		}

		$requestUri = 'https://overheid.io/api/kvk/';
		$requestUri .= $dossiernummer;

		$opts = array(
		  'http' => array(
		    'method' => "GET",
		    'header' =>
		        "ovio-api-key: ".$this->developerApiKey."\r\n"
		  )
		);

		if($respons = @file_get_contents($requestUri, false, stream_context_create($opts))) {
			$data = json_decode($respons);
			if(isset($data->_embedded->rechtspersoon[0])) {
				return $data->_embedded->rechtspersoon[0];
			}
		}

		return false;		
		
	}


}

?>