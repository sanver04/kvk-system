<?php

define('BASE_PATH', dirname(__FILE__));

include_once(BASE_PATH.'/config.php');
include_once(BASE_PATH.'/functions.php');
include_once(BASE_PATH.'/classes/KVKSystem.php');
include_once(BASE_PATH.'/classes/KVKSystem_Controller.php');
include_once(BASE_PATH.'/classes/View.php');
include_once(BASE_PATH.'/classes/Address.php');
include_once(BASE_PATH.'/classes/KVKApi.php');

$action = 'init';
if(isset($_REQUEST['action'])){
	$action = $_REQUEST['action'];
}

$params = array();
if(isset($_REQUEST)) {
	$params = $_REQUEST;
}


$model = new KVKSystem();
$controller = new KVKSystem_Controller($model, $action, $params);
$view = new View($model);

echo $view->output();

?>