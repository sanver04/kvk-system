<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>KVK System</title>
		<link rel="stylesheet" href="/assets/css/style.css" />
	</head>

	<body>
		
		<div id="framework">
			
			<header class="header">				
				<h1>KVK System</h1>
				<p>Voer een kvk nummer en vind het bijhorende adres.</p>

				<div class="form">					
					<form method="post">
						
						<input type="hidden" name="action" value="requestAddress" />

						<div class="field dossiernummer">
							<label for="dossiernummer">KVK nummer:</label>
							<input type="text" name="dossiernummer" id="dossiernummer" value="<?php echo (isset($_POST['dossiernummer'])) ? $_POST['dossiernummer'] : ''; ?>" />
						</div>					
						<div class="field submit">
							<button>Vind het adres</button>
						</div>
					</form>
				</div>

			</header>

			<div class="body">
				
				<?php

					if($this->Error) {
						echo $this->Error;
					} else {
						echo $this->Address;
					}
				?>

			</div>

		</div>

	</body>

</html>